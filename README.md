# ArchISO+ZFS ISO Project

Maintaining a version of ArchISO, but with ZFS support! WOO!

## ARCHISO+ZFS

Are you wanting ZFS with arch? We made it happen with some assistance from the ArchWiki!

## Authors and acknowledgment
Sprungles + ETJAKEOC
(if you commit and I accept the commit, I will add you here!)

## License
Check the LICENSE File. It is under multiple licenses.

## Understanding how to do this yourself
If you want to do this yourself go to [the Arch Wiki]("https://wiki.archlinux.org/title/ZFS") and it tells you how! Use the DKMS method. Or you can do how we did it, which is a LITTLE bit different.

## Project status
Project is going great! Will keep updating every month!
